#!/bin/bash

if (( $EUID != 0 )); then
  echo "Please run as sudo"
  exit
fi
curl -s https://packagecloud.io/install/repositories/varnishcache/varnish60lts/script.deb.sh | sudo bash

apt install -y varnish
service varnish stop

rm /etc/varnish/default.vcl
touch /etc/varnish/default.vcl
cat default.vcl | while read; do # "default.vcl må ligge i samme folder"
    echo "$REPLY" >> /etc/varnish/default.vcl
done




sudo mkdir /etc/systemd/system/varnish.service.d/
touch /etc/systemd/system/varnish.service.d/customexec.conf
echo "[Service]" >> /etc/systemd/system/varnish.service.d/customexec.conf
echo "ExecStart=" >> /etc/systemd/system/varnish.service.d/customexec.conf
echo "ExecStart=/usr/sbin/varnishd -a :80 -T localhost:6082 -f /etc/varnish/default.vcl -S /etc/varnish/secret -s default,256m" >> /etc/systemd/system/varnish.service.d/customexec.conf

systemctl daemon-reload
service varnish start

#*************OPS DENNE FUNKER IKKE 100%*********** INSTALLERER IKKE /etc/varnish/secret....***