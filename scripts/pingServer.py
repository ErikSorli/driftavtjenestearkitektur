import platform     #henter OS navn ved å benytte seg av flags( -c = UNIX)
import subprocess   #For executing a shell command
import sched, time #for å implementere en event scheduler


def ping(host):
    #return funksjon som returnerer true dersom host responderer til ping etterspørsel

    if platform.system().lower == "windows":
        param = "-n"
    else:
        param = "-c"
        command = ['ping', param, '1', host]
    return subprocess.call(command) #dersom ping er true return

def main():
    #host = openstack sine wbsider
    parameter = " 10.24.101.22"
    s = sched.scheduler(time.time, time.sleep)
    s.enter(2, 1, ping(parameter))
    #s.run()

main()
