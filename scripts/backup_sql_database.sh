#!/bin/bash

TIMESTAMP=$(date +%Y%m%d)
SQL_NAME="${TIMESTAMP}.sql" #bygger filnavn
cockroach dump bf --insecure > "/home/ubuntu/backups/$SQL_NAME" #dumper db

del=$(date --date="10 days ago" +%Y%m%d)
suffix=".sql"
for i in `find backups/ -type f -name "*.sql"`; do
  i=${i%$suffix}
  (($del > $(basename $i))) && echo "delete $i" || echo "dont delete $i"
done
