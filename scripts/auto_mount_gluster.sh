#!/bin/bash
#systemctl disable docker
#boot -> glusterd -> mount glusterfs -> systemctl start docker
STRING_ERROR="Har ventet i 60 sek og fikk ikke startet glusterfs $(hostname)"
STRING_GOOD="Da fikk jeg bootet opp glusterfs på $(hostname) . Bare hyggelig :)"
counter=0
while [ "$(df -h | grep -c /bf)" != 2 ]; do
        sudo mount -t glusterfs 192.168.129.211:/bf_config /bf_config
        sudo mount -t glusterfs 192.168.129.211:/bf_images /bf_images
        sleep 10
        ((counter+=1))
        if [ "$counter" -ge 6 ];then
                python3 /home/ubuntu/driftavtjenestearkitektur/scripts/bot/bot.py -s "$STRING_ERROR"
                exit 0
        fi
done

sudo systemctl start docker
python3 /home/ubuntu/driftavtjenestearkitektur/scripts/bot/bot.py -s "$STRING_GOOD"
