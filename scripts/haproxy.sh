#!/bin/bash

sudo add-apt-repository -y ppa:vbernat/haproxy-1.8

apt-get update
apt-get install -y haproxy socat

FRONEND="\nfrontend bookface\n\tbind *:80\n\tmode http\n\tdefault_backend nodes\n"
BACKEND="\nbackend nodes\n\tmode http\n\tbalance roundrobin\n\tserver www1 192.168.130.110:80 check\n\tserver web02 192.168.131.177:80 check\n"
ADMINCFG="\nlisten stats\n\tbind *:1936\n\tstats enable\n\tstats uri /\n\tstats hide-version\n\tstats auth void:password"

echo -e $FRONEND >> /etc/haproxy/haproxy.cfg
echo -e $BACKEND >> /etc/haproxy/haproxy.cfg
echo -e $ADMINCFG >> /etc/haproxy/haproxy.cfg