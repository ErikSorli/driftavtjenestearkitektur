#!/usr/bin/python3
import openstack
import os
import shutil

connection = openstack.connect(cloud='openstack', region_name='SkyHiGh')

policy_mapping = { # Disse må oppdateres:
    'docker-swarm': '/home/ubuntu/driftavtjenestearkitektur',
    'database':'/home/ubuntu/backups',
    'manager':'/home/ubuntu/scripts',
    'docker':'/home/ubuntu/driftavtjenestearkitektur',
    'varnish':'/etc/varnish/',
}

# 1: Get all server names
servers = connection.compute.servers()

# 2: Remove old file:
shutil.os.remove('backup_policy.conf')

# 3: Map server names with pre configured policy and write to file
with open('backup_policy.conf','w') as policyFile:

    for server in servers: #example: docker:/home/ubuntu
        if 'docker-swarm' in server.name:
            foldersToBackup = policy_mapping.get('docker-swarm') 
        else:
                foldersToBackup = policy_mapping.get(server.name)
        
        if server.name != 'backup':
            lineInConf = server.name + ":" + foldersToBackup
            policyFile.write(lineInConf+"\n")

