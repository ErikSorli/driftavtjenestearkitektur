#!/usr/bin/env bash                                                                 
if(($EUID != 0)); then                                                              
	echo"du må være sudo"
fi

#setting up the repository

apt-get remove docker docker-engine docker.io containerd runc
apt-get update update
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#adding fingerprint, and setting up the test environment

apt-key fingerprint 0EBFCD88
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
  stable nightly test"

#installing the docker
apt-get update

#installer en gitt version, dersom en ikke ønsker å installere den høyest mulig versjonen. 
#Dette kan være ønskelig fordi det ikke nødvendigvis kreves å kjøre den beste versjonen
#fordi vi ikke behøver så høy stabilitet

#apt-get install docker-ce=<VERSION_STRING> docker-ce-cli=<VERSION_STRING> containerd.io

apt-get install docker-ce docker-ce-cli containerd.io


#tester om det ble installert riktig kjør en 
#if(); then 
       docker run hello-world
#fi
