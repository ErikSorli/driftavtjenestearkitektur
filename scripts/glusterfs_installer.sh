#!/bin/bash
if (( $EUID != 0 )); then
  echo "Please run as sudo"
  exit
fi

apt-get -y install glusterfs-server glusterfs-client
systemctl enable glusterd
systemctl start glusterd

mkdir /bf_brick
mkdir /config_brick 

mkdir /bf_images
mkdir /bf_config
