#! /bin/bash 
#Array med alle instancene våre
INSTANCE[0]="varnish"
INSTANCE[1]="manager" 
INSTANCE[2]="docker"
INSTANCE[3]="database"
INSTANCE[4]="backup"
INSTANCE[5]="docker-swarm-1"
INSTANCE[6]="docker-swarm-2"
INSTANCE[7]="docker-swarm-3"
#sourcer programmet for autentisering
source /home/ubuntu/scripts/IMT3003_V20_group05-openrc.sh
#For loop for alle instancene som sjekker om de eksisterer
for tall in $(seq 0 7); do
#sjekker om instancene ligger i openstack
if openstack server list | grep ${INSTANCE[$tall]};
then
        echo "instancen: ${INSTANCE[$tall]} er ikke slettet"
else
        echo "funker ikke"
fi
done 
#for loop for alle instancene som sjekker om de er skrudd av
for tall in $(seq 0 7); do
#sjekker om de er skrudd av
if openstack server list | grep ${INSTANCE[$tall]} | grep ACTIVE;
then
        echo "instancen: ${INSTANCE[$tall]} er oppe"
else
        echo "nede restarter"
        python3 /home/ubuntu/driftavtjenestearkitektur/scripts/bot/bot.py -s "Kyrre har tatt den ned.. restarter ${INSTANCE[$tall]}"
        #starter instancene
        openstack server start ${INSTANCE[$tall]}
                #sleeper for å vente på at instancene skal boote(skitten kode vol1)
                sleep 1m
                #commands for å starte containere og cockroach
                # skitten kode vol2 gitt at de ikke blir sjekka om det er den instancen som starter.
                ssh ubuntu@192.168.131.110 'sudo docker start www5 www6 my-memcache'
                ssh ubuntu@192.168.131.110 'sudo docker ps'
                ssh ubuntu@192.168.133.38 'sudo cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background'
                ssh ubuntu@192.168.133.38 'sudo ps aux | grep cockroach'
fi
done 