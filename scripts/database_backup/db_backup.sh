#!/bin/bash

BACKUP_IP="192.168.131.176"

TIMESTAMP=$(date +%F_%T) #YYYY-MM-DD_HH:MM:SS
SQL_NAME_END="_backup.sql"
SQL_NAME="$TIMESTAMP$SQL_NAME_END" #bygger filnavn
cockroach dump bf --insecure > $SQL_NAME #dumper db
#OPS: denne vil drite på seg hvis det kommer flere sql filer den
# skal slette uten at scriptet er kjørt
rm -f $(ls -1t /home/ubuntu/backups | tail -n +11) #Sletter hvis det er over 10 stk .sql

scp -r /home/ubuntu/backups/$SQL_NAME ubuntu@$BACKUP_IP:/backup
ssh ubuntu@$BACKUP_IP /home/ubuntu/rydde_backup.sh

