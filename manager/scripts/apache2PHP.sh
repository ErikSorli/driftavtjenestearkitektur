#!/usr/bin/env bash

if (( $EUID != 0 )); then
  echo "Please run as sudo"
  exit
fi

# apt-get update
# apt-get -y install apache2
# apt-get -y install libapache2-mod-php
# apt-get -y install php-mysql
# apt-get -y install mysql-client

IP=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')
RESP_APACHE=$(wget -q -O - http://$IP/ | grep -o "It works!")
if [ "$RESP_APACHE" != "It works!" ]; then
	echo "ERROR: installation of apache2 or php"
	exit
fi
echo "<?php phpinfo(); ?>" > /var/www/html/installTest.php
service apache2 restart
RESP_PHP=$(wget -q -O - http://$IP/installTest.php | grep -o "PHP Credits")
if [ "$RESP_PHP" != "PHP Credits" ]; then
  echo "ERROR: installation of php"
  exit
fi
echo "php funket"

