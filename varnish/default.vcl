# See the VCL chapters in the Users Guide at https://www.varnish-cache.org/docs/
# and https://www.varnish-cache.org/trac/wiki/VCLExamples for more examples.

# Marker to tell the VCL compiler that this VCL has been adapted to the
# new 4.0 format.
vcl 4.0;
import directors;

backend server1 { #www1
    .host = "192.168.131.110";
    .port = "32770";
    .probe = { #health check
        .url = "/"; #vil denne url funke, eller må det være index.php?
        .expected_response = 200;
        .timeout = 1s;
        .interval = 3s;
        .window = 2;
        .threshold = 2;
        .initial = 2;
    }
}
backend server2 { #www2
    .host = "192.168.131.110";
    .port = "32771";
    .probe = { #health check
        .url = "/"; #vil denne url funke, eller må det være index.php?
        .expected_response = 200;
        .timeout = 1s;
        .interval = 3s;
        .window = 2;
        .threshold = 2;
        .initial = 2;
    }
}
backend server3 {
    .host = "192.168.131.110";
    .port = "32772";
    .probe = { #health check
        .url = "/"; #vil denne url funke, eller må det være index.php?
        .expected_response = 200;
        .timeout = 1s;
        .interval = 3s;
        .window = 2;
        .threshold = 2;
        .initial = 2;
    }
}
backend server4 {
    .host = "192.168.131.110";
    .port = "32773";
    .probe = { #health check
        .url = "/"; #vil denne url funke, eller må det være index.php?
        .expected_response = 200;
        .timeout = 1s;
        .interval = 3s;
        .window = 2;
        .threshold = 2;
        .initial = 2;
    }
}


sub vcl_init { #lastbalanserer
    new vdir = directors.round_robin();
    vdir.add_backend(server1);
    vdir.add_backend(server2);
    vdir.add_backend(server3);
    vdir.add_backend(server4);
}
sub vcl_recv {
    # Happens before we check if we have this in cache already.
    #
    # Typically you clean up the request here, removing cookies you don't need,
    # rewriting the request, etc.

        
# Properly handle different encoding types
        if (req.http.Accept-Encoding) {
                if (req.url ~ "\.(jpg|jpeg|png|gif|gz|tgz|bz2|tbz|mp3|ogg|swf|woff)$") {
                        # No point in compressing these
                        unset req.http.Accept-Encoding;
                } elsif (req.http.Accept-Encoding ~ "gzip") {
                        set req.http.Accept-Encoding = "gzip";
                } elsif (req.http.Accept-Encoding ~ "deflate") {
                        set req.http.Accept-Encoding = "deflate";
                } else {
                        # unknown algorithm (aka crappy browser)
                        unset req.http.Accept-Encoding;
                }
        }

        # Cache files with these extensions
        if (req.url ~ "\.(js|showimage.php?user=|css|jpg|jpeg|png|gif|gz|tgz|bz2|tbz|mp3|ogg|swf|woff)$") {
                unset req.http.cookie;
                return (hash);
        }

        # Dont cache anything thats on the blog page or thats a POST request
        if (req.url ~ "^/blog" || req.method == "POST") {
                return (pass);
        }

        # This is Laravel specific, we have session-monster which sets a no-session header if we dont really need the set session cookie. 
        # Check for this and unset the cookies if not required
        # Except if its a POST request
        if (req.http.X-No-Session ~ "yeah" && req.method != "POST") {
                unset req.http.cookie;
        }

        return (hash);
}

sub vcl_backend_response {
    # Happens after we have read the response headers from the backend.
    #
    # Here you clean the response headers, removing silly Set-Cookie headers
    # and other mistakes your backend does.

        # This is how long Varnish will cache content. Set at top for visibility.
        set beresp.ttl = 1d;

        if ((bereq.method == "GET" && bereq.url ~ "\.(css|showimage.php?user=|js|xml|gif|jpg|jpeg|swf|png|zip|ico|img|wmf|txt)$") ||
                bereq.url ~ "\.(minify).*\.(css|js).*" ||
                bereq.url ~ "\.(css|showimage.php?user=|js|xml|gif|jpg|jpeg|swf|png|zip|ico|img|wmf|txt)\?ver") {
                unset beresp.http.Set-Cookie;
                set beresp.ttl = 5d;
        }

        # Unset all cache control headers bar Age.
        unset beresp.http.etag;
        unset beresp.http.Cache-Control;
        unset beresp.http.Pragma;

        # Unset headers we never want someone to see on the front end
        unset beresp.http.Server;
        unset beresp.http.X-Powered-By;

        # Set how long the client should keep the item by default
        set beresp.http.cache-control = "max-age = 300";

        # Set how long the client should keep the item by default
        set beresp.http.cache-control = "max-age = 300";

        # Override browsers to keep styling and dynamics for longer
        if (bereq.url ~ ".minify.*\.(css|js).*") { set beresp.http.cache-control = "max-age = 604800"; }
        if (bereq.url ~ "\.(css|js).*") { set beresp.http.cache-control = "max-age = 604800"; }

        # Override the browsers to cache longer for images than for main content        
        if (bereq.url ~ ".(xml|gif|jpg|jpeg|swf|css|js|showimage.php?user=|png|zip|ico|img|wmf|txt)$") {
                set beresp.http.cache-control = "max-age = 604800";
        }

       # We're done here, send the data to the browser
        return (deliver);
}


sub vcl_deliver {
    # Happens when we have all the pieces we need, and are about to send the
    # response to the client.
    #
    # You can do accounting or modifying the final object here.

        # Lets not tell the world we are using Varnish in the same principle we set server_tokens off in Nginx
        unset resp.http.Via;
        unset resp.http.X-Varnish;
}
